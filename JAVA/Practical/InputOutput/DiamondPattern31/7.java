import java.util.*;
class Demo7{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row = sc.nextInt();

		int sp =0;
		int col=0;
		
		for(int i =1;i<=2*row-1;i++){
			int num =65;
			if(i<=row){
				sp = row-i;
				col=2*i-1;
			}else{
				sp = i-row;
				col -= 2; 
			}
			for(int j=1;j<=sp;j++){
				System.out.print("\t");
			}
			for(int k=1;k<=col;k++){
				if(k<=col/2){
					System.out.print((char)num++ +"\t");
				}else{
					System.out.print((char)num-- + "\t" );
				}
			}
			System.out.println();
			
		}
	}
}
