import java.util.*;
class Demo10{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row = sc.nextInt();
		System.out.print("Enter number of columns : ");
		int col = sc.nextInt();

		int arr [] [] = new int [row][col];

		
		for(int i =0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j] = sc.nextInt();
			}
		}
		for(int i =0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				if(i%(row-1)==0 && j%(row-1)==0){
					System.out.print(arr[i][j]+",");
				}
			}
		}
			System.out.println();
	}
}
