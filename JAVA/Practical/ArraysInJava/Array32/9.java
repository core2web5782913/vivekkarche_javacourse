import java.util.*;
class Demo9{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row = sc.nextInt();
		System.out.print("Enter number of columns : ");
		int col = sc.nextInt();

		int arr [] [] = new int [row][col];

		
		for(int i =0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j] = sc.nextInt();
			}
		}
		int sum2 = 0;
		int sum1=0;
		for(int i =0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				if((i+j)==(row-1)){
					sum2+=arr[i][j];
				}
				if(i==j){
					sum1+=arr[i][j];
				}
			}
		}
			System.out.println("PRODUCT OF SUM OF PRIMARY AND  SECONDARY  DIAGONAL : "+sum1*sum2);
	}
}
