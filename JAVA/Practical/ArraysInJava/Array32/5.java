import java.util.*;
class Demo5{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row = sc.nextInt();
		System.out.print("Enter number of columns : ");
		int col = sc.nextInt();

		int arr [] [] = new int [row][col];

		
		for(int i =0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j] = sc.nextInt();
			}
		}
		int arr1 [] = new int [col];
		for(int i =0;i<col;i++){
			for(int j=0;j<row;j++){
				arr1[i]+= arr[j][i];
			}
			System.out.println("Sum of col "+(i+1)+" = "+arr1[i]);
		}
		
	}
}
