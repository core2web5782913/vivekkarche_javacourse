import java.util.*;
class Demo4{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number of rows : ");
		int row = sc.nextInt();
		System.out.print("Enter number of columns : ");
		int col = sc.nextInt();

		int arr [] [] = new int [row][col];

		
		for(int i =0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j] = sc.nextInt();
			}
		}
		int num =1;	
		for(int i =0;i<arr.length;i++){
			int sum =0;
			if(num%2==1){
				for(int j=0;j<arr[i].length;j++){
					sum = sum + arr[i][j];
				}
			
		
				System.out.println("sum of row "+ num+ " = "+sum);	
			}
			num++;
		}
	}
}
