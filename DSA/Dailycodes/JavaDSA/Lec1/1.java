import java.util.*;
class Demo1{

	static int factors(int num ){
		int count = 0;
		for(int i=1;i<=num/2;i++){
			if(num%i==0){
				count++;
			}
		}
		return count+1;
	}
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter any number you have to find the factors count : ");
		int num = sc.nextInt();

		int count = factors(num);
		System.out.println(count);
				
/*		int count =0;
		for(int i=1;i<=num;i++){
			if(num%i==0){
				count++;
			}
		}
		System.out.println("Number of Factors of "+num + " is " + count);
*/
	}
}
