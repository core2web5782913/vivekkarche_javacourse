package main;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class App extends Application{
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        launch(args);
    }

    @Override
    public void start (Stage prStage ){
        prStage.setTitle("MyStage");
        prStage.setHeight(800);
        prStage.setWidth(500);
        prStage.getIcons().add(new Image ("assets/images/assets.png"));
        prStage.show();
    }
}
