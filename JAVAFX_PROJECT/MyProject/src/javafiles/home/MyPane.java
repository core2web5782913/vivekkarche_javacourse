package javafiles.home;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class MyPane extends Application {

    @Override
    public void start(Stage st) {

            Label lb = new Label("Good Morning");
           
            
            BorderPane bp = new BorderPane(lb);
            bp.setAlignment(lb, Pos.TOP_CENTER);
            
            // Group gr = new Group(bp);
            Scene sc = new Scene(bp);
            
            st.setScene(sc);
            st.setTitle("MyPane Project");
            st.setHeight(800);
            st.setWidth(500);

            

            st.show();

    }
    
}
