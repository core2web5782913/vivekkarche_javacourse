package javafiles.home;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class App extends Application {
    

    @Override
    public void start(Stage Stage) {

            Text t1 = new Text(10, 30, "Employee Details of Company");
            t1.setFont(new Font(20));
            t1.setFill(Color.RED);
            // t1.setTextAlignment(TextAlignment.CENTER);
            

            Text name = new Text(15,50,"NAME ");
            name.setFont(new Font(15));
            name.setFill(Color.BROWN);
            Text id = new Text(15,80,"ID");
            id.setFont(new Font(15));
            id.setFill(Color.BROWN);
            Text salary = new Text(15,110,"SALARY");
            salary.setFont(new Font(15));
            salary.setFill(Color.BROWN);

            VBox vb = new VBox(30,name,id,salary);
            vb.setLayoutX(15);
            vb.setLayoutY(70);

            // vb.setBackground(new Background(new BackgroundFill(
            //     Color.web("#3498db"), CornerRadii.EMPTY, Insets.EMPTY)));

            Text name1 = new Text(30,50,"Vivek");
            name1.setFont(new Font(15));
            name1.setFill(Color.BLUEVIOLET);
            Text id1 = new Text("1436345");
            id1.setFont(new Font(15));
            id1.setFill(Color.BLUEVIOLET);
            Text salary1 = new Text("1.2Lac");
            salary1.setFont(new Font(15));
            salary1.setFill(Color.BLUEVIOLET);

            VBox vb1 = new VBox(30,name1,id1,salary1);
            vb1.setLayoutX(15);
            vb1.setLayoutY(205);

            //   vb1.setBackground(new Background(new BackgroundFill(
            //   Color.web("#3498db"), CornerRadii.EMPTY, Insets.EMPTY)));

            HBox hb = new HBox(20,vb,vb1);
            hb.setLayoutX(20);
            hb.setLayoutY(60);
            // hb.setAlignment(Pos.TOP_CENTER);
            HBox hb1 =  new HBox(t1);
            hb1.setLayoutX(10);
            hb1.setLayoutY(10);
            hb1.setAlignment(Pos.TOP_CENTER);

            Group gr = new Group(hb1,hb);

            Scene sc = new Scene(gr);
            sc.setFill(Color.AQUA);
            // sc.setCursor(Cursor.OPEN_HAND);
            Stage.setScene(sc);

            Stage.setTitle("VK Company");
            Stage.getIcons().add(new Image("assets/images/vklogo.png"));
            Stage.setHeight(800);
            Stage.setWidth(500);
            Stage.setX(0);
            Stage.setY(0);
            
           // Stage.centerOnScreen();
            Stage.show();
        
    }
}
