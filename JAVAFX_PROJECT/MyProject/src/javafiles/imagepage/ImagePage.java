package javafiles.imagepage;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ImagePage extends Application {

    @Override
    public void start(Stage ImStage) {
            
            Image ig = new Image("assets/images/images.jpeg");
            ImageView iv = new ImageView(ig);
            iv.setFitHeight(200);
            iv.setFitWidth(300);
            iv.setPreserveRatio(true);

            VBox vb1 = new VBox(iv);
         
           
            
            
            Label lb = new Label("DSA");
            lb.setFont(new Font(25));
            lb.setStyle("-fx-background-Color:Yellow");

            VBox vb2 = new VBox(lb);
            
            vb2.setPrefWidth(300);
            vb2.setAlignment(Pos.CENTER);
            vb2.setStyle("-fx-background-Color:ORANGE");
          
            
            HBox hb = new HBox(vb1,vb2);
            
            // hb.setPrefHeight(200);
            hb.setPrefWidth(600);
            // hb.setLayoutX(600);
            // hb.setLayoutX(00);
           // hb.setStyle("-fx-background-Color:ORANGE");
            
            Group gr = new Group(hb);

            Scene sc = new Scene (gr);
            sc.setFill(Color.AQUA);
            ImStage.setScene(sc);

            ImStage.setTitle("ImageView");
            ImStage.setHeight(1000);
            ImStage.setWidth(1400);
            ImStage.setX(0);
            ImStage.setY(0);
            ImStage.show();

    }
    
}
